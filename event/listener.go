package event

import (
	"context"
	"log"
	"time"

	"bitbucket.org/nick_hultz/edge-swe-challenge/db"
	pb "bitbucket.org/nick_hultz/edge-swe-challenge/gen"
	"go.uber.org/zap"

	"github.com/nats-io/nats.go"
	"google.golang.org/protobuf/proto"
)

// All supported event feeds
const (
	ExecutionFeed  = "execution"
	SportEventFeed = "sport_event"
)

// Listener is an abstraction over all event feeds.
type Listener struct {
	logger         *zap.Logger
	eventStore     *db.EventStore
	natsClient     *nats.Conn
	subscriptions  []*nats.Subscription
	ExecutionChan  chan *pb.Execution
	SportEventChan chan *pb.Event
}

// NewListener creates a new event listener configured
// with an event store to persist all incoming events.
func NewListener(es *db.EventStore, logger *zap.Logger) *Listener {
	return &Listener{
		logger:         logger,
		eventStore:     es,
		natsClient:     nil,
		subscriptions:  []*nats.Subscription{},
		ExecutionChan:  make(chan *pb.Execution),
		SportEventChan: make(chan *pb.Event),
	}
}

func (l *Listener) setupConnOptions() []nats.Option {
	totalWait := 10 * time.Minute
	reconnectDelay := time.Second

	opts := []nats.Option{}

	opts = append(opts, nats.ReconnectWait(reconnectDelay))
	opts = append(opts, nats.MaxReconnects(int(totalWait/reconnectDelay)))
	opts = append(opts, nats.DisconnectErrHandler(func(nc *nats.Conn, err error) {
		if err != nil {
			l.logger.Sugar().Infof("nats client disconnected due to: %s, will attempt reconnects for %.0fm",
				err, totalWait.Minutes(),
			)
		}
	}))
	opts = append(opts, nats.ReconnectHandler(func(nc *nats.Conn) {
		l.logger.Sugar().Infof("nats client reconnected [%s]", nc.ConnectedUrl())
	}))
	opts = append(opts, nats.ClosedHandler(func(nc *nats.Conn) {
		if nc.LastError() != nil {
			l.logger.Sugar().Infof("nats client exiting: %v", nc.LastError())
		}
	}))
	return opts
}

// StartEventFeeds will connect to a NATS server and attempt to
// subscribe to all event feeds. Each feed broadcasts events
// through a channel to allow the server to listen in for new events.
func (l *Listener) StartEventFeeds(url string) {
	opts := l.setupConnOptions()

	nc, err := nats.Connect(url, opts...)
	if err != nil {
		l.logger.Sugar().Errorw("unable to connect to NATS server",
			"err", err,
		)
		return
	}
	l.natsClient = nc

	executionSub, err := l.natsClient.Subscribe(ExecutionFeed, l.handleExecution)
	if err != nil {
		l.logger.Sugar().Errorw("unable to subscribe to event feed",
			"feed", ExecutionFeed,
			"err", err,
		)

	} else {
		l.subscriptions = append(l.subscriptions, executionSub)
	}

	sportSub, err := l.natsClient.Subscribe(SportEventFeed, l.handleSportEvent)
	if err != nil {
		l.logger.Sugar().Errorw("unable to subscribe to event feed",
			"feed", SportEventFeed,
			"err", err,
		)
	} else {
		l.subscriptions = append(l.subscriptions, sportSub)
	}
}

// Shutdown will gracefully unsubscribe from all open subscriptions,
// close all outgoing event channels, and close the NATS client.
func (l *Listener) Shutdown(ctx context.Context) {
	l.logger.Sugar().Info("shutting down event subscriptions")

	for _, sub := range l.subscriptions {
		if err := sub.Drain(); err != nil {
			log.Printf("error closing %q subscription: %v", sub.Subject, err)
			l.logger.Sugar().Errorw("error closing subscription",
				"feed", sub.Subject,
				"err", err,
			)
		}
	}

	close(l.ExecutionChan)
	close(l.SportEventChan)
	l.logger.Sugar().Info("event broadcast channels closed")

	l.natsClient.Close()
	l.logger.Sugar().Info("NATS client closed")

	<-ctx.Done()
}

func (l *Listener) handleExecution(m *nats.Msg) {
	execution := &pb.Execution{}
	if err := proto.Unmarshal(m.Data, execution); err != nil {
		l.logger.Sugar().Errorw("failed to parse message from feed",
			"feed", ExecutionFeed,
			"err", err,
		)
		return
	}

	err := l.eventStore.SaveExecution(execution)
	if err != nil {
		l.logger.Sugar().Errorw("failed to save message from feed",
			"feed", ExecutionFeed,
			"err", err,
		)
		return
	}

	l.ExecutionChan <- execution
}

func (l *Listener) handleSportEvent(m *nats.Msg) {
	sportEvent := &pb.Event{}
	if err := proto.Unmarshal(m.Data, sportEvent); err != nil {
		l.logger.Sugar().Errorw("failed to parse message from feed",
			"feed", SportEventFeed,
			"err", err,
		)
		return
	}

	err := l.eventStore.SaveSportEvent(sportEvent)
	if err != nil {
		l.logger.Sugar().Errorw("failed to save message from feed",
			"feed", SportEventFeed,
			"err", err,
		)
		return
	}

	l.SportEventChan <- sportEvent
}
