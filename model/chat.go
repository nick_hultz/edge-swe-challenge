package model

// Message represents a row in the messages table.
type Message struct {
	ID      int
	Content string
	Room    Room
}

// Room represents a row in the rooms table.
type Room struct {
	ID      int
	Name    string
	Filters []Filter
}

// Filter represents a row in the filters table.
type Filter struct {
	ID          int
	FeedType    string
	FilterType  string
	Field       string
	FilterValue string
	Active      bool
}
