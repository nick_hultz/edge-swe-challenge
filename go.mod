module bitbucket.org/nick_hultz/edge-swe-challenge

go 1.14

require (
	github.com/golang-migrate/migrate/v4 v4.11.0
	github.com/golang/protobuf v1.4.1
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/jpillora/backoff v1.0.0
	github.com/lib/pq v1.7.1
	github.com/nats-io/nats-server/v2 v2.1.7 // indirect
	github.com/nats-io/nats.go v1.10.0
	go.uber.org/zap v1.15.0
	google.golang.org/protobuf v1.25.0
)
