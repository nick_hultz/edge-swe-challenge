package config

import (
	"os"
	"strconv"
)

// DatabaseConfig provides an abstraction for all database configuration.
type DatabaseConfig struct {
	Host     string
	Port     int
	User     string
	Password string
	DbName   string
}

// NatsConfig provides an abstraction for all NATS configuration.
type NatsConfig struct {
	URL string
}

// Config provides an abstraction for all project configuration.
type Config struct {
	Port     int
	Database DatabaseConfig
	Nats     NatsConfig
}

// New will parse the current environment and create a Config instance
// for the project.
func New() *Config {
	return &Config{
		Port: getEnvAsInt("PORT", 8080),
		Database: DatabaseConfig{
			Host:     getEnv("DB_HOST", "localhost"),
			Port:     getEnvAsInt("DB_PORT", 5432),
			User:     getEnv("DB_USER", "chat_svc"),
			Password: getEnv("DB_PASS", "password"),
			DbName:   getEnv("DB_NAME", "chat"),
		},
		Nats: NatsConfig{
			URL: getEnv("NATS_URL", "127.0.0.1:4222"),
		},
	}
}

func getEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}

	return defaultVal
}

func getEnvAsInt(name string, defaultVal int) int {
	valueStr := getEnv(name, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}

	return defaultVal
}
