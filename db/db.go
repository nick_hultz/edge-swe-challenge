package db

import (
	"database/sql"
	"fmt"
	"time"

	"bitbucket.org/nick_hultz/edge-swe-challenge/config"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/jpillora/backoff"
	"go.uber.org/zap"

	// Blank import needed to bring in migration file functionality
	_ "github.com/golang-migrate/migrate/v4/source/file"
	// Blank import needed to bring in postgres driver functionality
	_ "github.com/lib/pq"
)

// New creates a new database connection and makes sure
// the connection works.  It will ping the database with
// exponential backoff until a successful connection is made.
func New(cfg config.DatabaseConfig, logger *zap.Logger) (*sql.DB, error) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.User, cfg.Password, cfg.DbName,
	)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}

	pingWithBackoff(db, logger)

	logger.Sugar().Info("database ping successful")
	return db, nil
}

func pingWithBackoff(db *sql.DB, logger *zap.Logger) {
	b := &backoff.Backoff{
		Min:    1 * time.Second,
		Max:    2 * time.Minute,
		Jitter: true,
	}

	for {
		err := db.Ping()
		if err != nil {
			d := b.Duration()
			logger.Sugar().Infof("database ping failed. Sleeping for %s", d)
			time.Sleep(d)
			continue
		}

		b.Reset()
		break
	}
}

// AutoMigrate compares the current version of the database
// with the migration version and brings everthing up to date.
func AutoMigrate(db *sql.DB) error {
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return err
	}

	m, err := migrate.NewWithDatabaseInstance("file://migrations", "postgres", driver)
	if err != nil {
		return err
	}

	if err := m.Up(); err != nil {
		// ErrNoChange is expected when no new migrations are found.
		if err == migrate.ErrNoChange {
			return nil
		}
		return err
	}

	return nil
}
