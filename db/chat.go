package db

import (
	"context"
	"database/sql"

	"bitbucket.org/nick_hultz/edge-swe-challenge/model"
)

// ChatStore provides a database abstraction for the chat tables.
type ChatStore struct {
	db *sql.DB
}

// NewChatStore creates an ChatStore configured
// with a database connection.
func NewChatStore(db *sql.DB) *ChatStore {
	return &ChatStore{
		db: db,
	}
}

// SaveMessage persists a new chat message into the database.
func (cs *ChatStore) SaveMessage(clientID string, content string, roomID int) error {
	insertMessage := `
		insert into messages (
			user_name, content, room_id
		) values (
			$1, $2, $3
		)
	`
	_, err := cs.db.Exec(insertMessage, clientID, content, roomID)
	if err != nil {
		return err
	}

	return nil
}

// GetRooms retrieves all configured rooms.
func (cs *ChatStore) GetRooms() ([]model.Room, error) {
	selectRooms := `
		select id, name
		from rooms
	`
	rows, err := cs.db.Query(selectRooms)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var rooms []model.Room
	for rows.Next() {
		var r model.Room
		err = rows.Scan(&r.ID, &r.Name)
		if err != nil {
			return nil, err
		}
		rooms = append(rooms, r)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return rooms, nil
}

// GetActiveFilters retrieves all of the currently active filters
// for the provided room.
func (cs *ChatStore) GetActiveFilters(roomID int) ([]model.Filter, error) {
	selectFilters := `
		select feed_type, filter_type, field, filter_value
		from filters
		where active = 'true'
		and room_id = $1
	`
	rows, err := cs.db.Query(selectFilters, roomID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var filters []model.Filter
	for rows.Next() {
		var f model.Filter
		err = rows.Scan(&f.FeedType, &f.FilterType, &f.Field, &f.FilterValue)
		if err != nil {
			return nil, err
		}
		filters = append(filters, f)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return filters, nil
}

// SaveFilters will deactivate all current filters for a room
// and save the new configuration.
func (cs *ChatStore) SaveFilters(roomID int, filters []model.Filter) error {
	turnOffCurrentFilters := `
		update filters
		set active = 'false'
		where active = 'true'
		and room_id = $1
	`

	insertFilter := `
		insert into filters (
			room_id, feed_type, filter_type, field, filter_value, active
		) values (
			$1, $2, $3, $4, $5, $6
		)
	`

	ctx := context.Background()
	tx, err := cs.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(ctx, turnOffCurrentFilters, roomID)
	if err != nil {
		tx.Rollback()
		return err
	}

	for _, filter := range filters {
		_, err := cs.db.Exec(
			insertFilter,
			roomID,
			filter.FeedType,
			filter.FilterType,
			filter.Field,
			filter.FilterValue,
			true,
		)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}
