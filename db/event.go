package db

import (
	"database/sql"
	"time"

	pb "bitbucket.org/nick_hultz/edge-swe-challenge/gen"
)

// EventStore provides a database abstraction for the event tables.
type EventStore struct {
	db *sql.DB
}

// NewEventStore creates an EventStore configured
// with a database connection.
func NewEventStore(db *sql.DB) *EventStore {
	return &EventStore{
		db: db,
	}
}

// SaveExecution persists a new Execution into the database.
func (es *EventStore) SaveExecution(e *pb.Execution) error {
	insertStmt := `
		insert into feed_executions (
			symbol, market, price, quantity, state_symbol, time_stamp
		) values (
			$1, $2, $3, $4, $5, $6
		)
	`
	_, err := es.db.Exec(insertStmt,
		e.Symbol,
		e.Market,
		e.Price,
		e.Quantity,
		e.StateSymbol,
		e.ExecutionEpoch,
	)
	if err != nil {
		return err
	}

	return nil
}

// SaveSportEvent persists a new SportEvent into the database.
func (es *EventStore) SaveSportEvent(s *pb.Event) error {
	insertStmt := `
		insert into feed_sport_events (
			sport, match_title, data_event, time_stamp
		) values (
			$1, $2, $3, $4
		)
	`
	now := time.Now()

	_, err := es.db.Exec(insertStmt,
		s.Sport.String(),
		s.MatchTitle,
		s.DataEvent,
		now.Unix(),
	)
	if err != nil {
		return err
	}

	return nil
}
