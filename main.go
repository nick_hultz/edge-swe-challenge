package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/nick_hultz/edge-swe-challenge/chat"
	"bitbucket.org/nick_hultz/edge-swe-challenge/config"
	"bitbucket.org/nick_hultz/edge-swe-challenge/db"
	"bitbucket.org/nick_hultz/edge-swe-challenge/event"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

func main() {
	cfg := config.New()

	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("unable to create a logger instance: %v", err)
	}
	defer logger.Sync()

	dbConn, err := db.New(cfg.Database, logger)
	if err != nil {
		logger.Sugar().Errorw("unable to create a new database connection",
			"err", err,
		)
		return
	}
	defer dbConn.Close()

	if err = db.AutoMigrate(dbConn); err != nil {
		logger.Sugar().Errorw("unable to run database migrations",
			"err", err,
		)
		return
	}

	cs := db.NewChatStore(dbConn)
	es := db.NewEventStore(dbConn)

	l := event.NewListener(es, logger)
	l.StartEventFeeds(cfg.Nats.URL)

	chatServer := chat.NewServer(cs, l, logger)
	go chatServer.Run()

	r := mux.NewRouter()

	r.Path("/chat/{roomName}").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		roomName := vars["roomName"]

		if err := chatServer.CheckRoomExists(roomName); err != nil {
			logger.Sugar().Error(err)
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}

		if err := chat.ConnectChatClient(logger, chatServer, roomName, w, r); err != nil {
			logger.Sugar().Errorw("unable to create a websocket client",
				"err", err,
			)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	})

	srv := &http.Server{
		Addr:         fmt.Sprintf("0.0.0.0:%d", cfg.Port),
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}

	go func() {
		logger.Sugar().Infof("http server started on port %d", cfg.Port)
		if err := srv.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				logger.Sugar().Fatal(err)
			}
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block until we receive an interupt signal.
	<-c

	// Wait max 10 seconds before completely shutting down.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	l.Shutdown(ctx)
	srv.Shutdown(ctx)

	logger.Sugar().Info("http server stopped")
	os.Exit(0)
}
