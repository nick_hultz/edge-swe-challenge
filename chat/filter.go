package chat

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/nick_hultz/edge-swe-challenge/event"
	pb "bitbucket.org/nick_hultz/edge-swe-challenge/gen"
	"bitbucket.org/nick_hultz/edge-swe-challenge/model"
)

type executionFilter func(*pb.Execution) bool

type sportEventFilter func(*pb.Event) bool

const (
	enabledFilter = "enabled"
	existsFilter  = "exists"
	maxFilter     = "max"
)

func (c *executionFilterConfig) buildFilters() []model.Filter {
	filters := []model.Filter{
		{
			FeedType:    event.ExecutionFeed,
			FilterType:  enabledFilter,
			Field:       "enabled",
			FilterValue: strconv.FormatBool(c.Enabled),
		},
	}

	if len(c.Markets) != 0 {
		validMarkets := strings.Join(c.Markets, ",")
		filters = append(filters, model.Filter{
			FeedType:    event.ExecutionFeed,
			FilterType:  existsFilter,
			Field:       "market",
			FilterValue: validMarkets,
		})
	}

	if c.MaxPrice != 0 {
		filters = append(filters, model.Filter{
			FeedType:    event.ExecutionFeed,
			FilterType:  maxFilter,
			Field:       "price",
			FilterValue: fmt.Sprintf("%f", c.MaxPrice),
		})
	}

	return filters
}

func (c *sportEventFilterConfig) buildFilters() []model.Filter {
	filters := []model.Filter{
		{
			FeedType:    event.SportEventFeed,
			FilterType:  enabledFilter,
			Field:       "enabled",
			FilterValue: strconv.FormatBool(c.Enabled),
		},
	}

	if len(c.Sports) != 0 {
		validSports := strings.Join(c.Sports, ",")
		filters = append(filters, model.Filter{
			FeedType:    event.SportEventFeed,
			FilterType:  existsFilter,
			Field:       "sport",
			FilterValue: validSports,
		})
	}

	return filters
}

func makeExecutionFilter(f model.Filter) (executionFilter, error) {
	switch f.FilterType {
	case enabledFilter:
		flag, err := strconv.ParseBool(f.FilterValue)
		if err != nil {
			return nil, err
		}
		return makeEnabledExecutionFilter(flag), nil

	case existsFilter:
		validValues := make(map[string]bool)
		for _, val := range strings.Split(f.FilterValue, ",") {
			validValues[val] = true
		}

		var getFunc func(e *pb.Execution) string
		switch f.Field {
		case "market":
			getFunc = func(e *pb.Execution) string { return e.GetMarket() }
		case "state":
			getFunc = func(e *pb.Execution) string { return e.GetStateSymbol() }
		default:
			return nil, fmt.Errorf("unable to create an exists filter for unknown field %q", f.Field)
		}

		return makeExistsExecutionFilter(validValues, getFunc), nil

	case maxFilter:
		threshold, err := strconv.ParseFloat(f.FilterValue, 32)
		if err != nil {
			return nil, err
		}

		var getFunc func(e *pb.Execution) float32
		switch f.Field {
		case "price":
			getFunc = func(e *pb.Execution) float32 { return e.GetPrice() }
		default:
			return nil, fmt.Errorf("unable to create a max filter for unknown field %q", f.Field)
		}

		return makeMaxValExecutionFilter(float32(threshold), getFunc), nil

	default:
		return nil, fmt.Errorf("unable to create an %q filter for unknown filter type %q", event.ExecutionFeed, f.FilterType)
	}
}

func makeSportEventFilter(f model.Filter) (sportEventFilter, error) {
	switch f.FilterType {
	case enabledFilter:
		flag, err := strconv.ParseBool(f.FilterValue)
		if err != nil {
			return nil, err
		}

		return makeEnabledSportEventFilter(flag), nil

	case existsFilter:
		validValues := make(map[string]bool)
		for _, val := range strings.Split(f.FilterValue, ",") {
			validValues[val] = true
		}

		var getFunc func(e *pb.Event) string
		switch f.Field {
		case "sport":
			getFunc = func(e *pb.Event) string { return e.Sport.String() }
		default:
			return nil, fmt.Errorf("unable to create an exists filter for unknown field %q", f.Field)
		}

		return makeExistsSportEventFilter(validValues, getFunc), nil

	default:
		return nil, fmt.Errorf("unable to create an %q filter for unknown filter type %q", event.SportEventFeed, f.FilterType)
	}
}

func makeEnabledExecutionFilter(enabledFlag bool) executionFilter {
	return func(e *pb.Execution) bool {
		return enabledFlag
	}
}

func makeExistsExecutionFilter(validVals map[string]bool, getFunc func(e *pb.Execution) string) executionFilter {
	return func(e *pb.Execution) bool {
		val := strings.ToLower(getFunc(e))
		if _, exists := validVals[val]; !exists {
			return false
		}
		return true
	}
}

func makeMaxValExecutionFilter(maxThreshold float32, getFunc func(e *pb.Execution) float32) executionFilter {
	return func(e *pb.Execution) bool {
		return getFunc(e) < maxThreshold
	}
}

func makeEnabledSportEventFilter(enabledFlag bool) sportEventFilter {
	return func(e *pb.Event) bool {
		return enabledFlag
	}
}

func makeExistsSportEventFilter(validVals map[string]bool, getFunc func(e *pb.Event) string) sportEventFilter {
	return func(e *pb.Event) bool {
		val := strings.ToLower(getFunc(e))
		if _, exists := validVals[val]; !exists {
			return false
		}
		return true
	}
}
