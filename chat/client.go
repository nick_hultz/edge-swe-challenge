package chat

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	expectedWebSocketErrors = []int{
		websocket.CloseGoingAway,
		websocket.CloseAbnormalClosure,
		websocket.CloseNoStatusReceived,
	}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	// This CORS config is only to ensure testing / validation can occur.
	// A real-production app would have origins correctly set to validate
	// incoming requests.
	CheckOrigin: func(r *http.Request) bool { return true },
}

const (
	sendMessageType  = "sendMessage"
	changeFilterType = "changeFilter"
)

type webSocketMessage struct {
	Type   string          `json:"type"`
	MsgRaw json.RawMessage `json:"msg"`
}

type sendMessage struct {
	RoomName string `json:"room,omitempty"`
	Content  string `json:"content,omitempty"`
	ClientID string `json:"client,omitempty"`
	Err      string `json:"err,omitempty"`
}

type changeFilter struct {
	ClientID         string                 `json:"client"`
	RoomName         string                 `json:"room"`
	ExecutionConfig  executionFilterConfig  `json:"execution"`
	SportEventConfig sportEventFilterConfig `json:"sportEvent"`
}

type executionFilterConfig struct {
	Enabled  bool     `json:"enabled"`
	Markets  []string `json:"markets"`
	MaxPrice float32  `json:"maxPrice"`
}

type sportEventFilterConfig struct {
	Enabled bool     `json:"enabled"`
	Sports  []string `json:"sports"`
}

// Client is a wrapper around the live websocket connection.
// All reads / writes of the websocket connection happen
// through this chat client.
type Client struct {
	logger      *zap.Logger
	id          string
	conn        *websocket.Conn
	server      *Server
	currentRoom string
	send        chan sendMessage
}

func (c *Client) readFromSocket() {
	defer func() {
		sub := subscription{
			roomName: c.currentRoom,
			client:   c,
		}
		c.server.leaveChan <- sub
		c.conn.Close()
	}()

	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })

	for {
		_, data, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, expectedWebSocketErrors...) {
				c.logger.Sugar().Errorw("unexpected websocket error when reading message",
					"err", err,
				)
			}
			break
		}

		var msg webSocketMessage
		if err := json.Unmarshal(data, &msg); err != nil {
			c.handleWebsocketError(fmt.Errorf("unable to parse json: %w", err))
			continue
		}

		switch msg.Type {
		case sendMessageType:
			var sendMessage sendMessage
			if err := json.Unmarshal(msg.MsgRaw, &sendMessage); err != nil {
				c.handleWebsocketError(fmt.Errorf("unable to parse json: %w", err))
				continue
			}

			sendMessage.ClientID = c.id
			sendMessage.RoomName = c.currentRoom

			c.server.userBroadcast <- sendMessage

		case changeFilterType:
			var changeFilter changeFilter
			if err := json.Unmarshal(msg.MsgRaw, &changeFilter); err != nil {
				c.handleWebsocketError(fmt.Errorf("unable to parse json: %w", err))
				break
			}

			changeFilter.ClientID = c.id
			changeFilter.RoomName = c.currentRoom

			c.server.filterChan <- changeFilter
		}
	}
}

func (c *Client) writeToSocket() {
	keepAliveTicker := time.NewTicker(pingPeriod)
	defer func() {
		keepAliveTicker.Stop()
		c.conn.Close()
	}()

	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The server closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			if err := c.conn.WriteJSON(message); err != nil {
				c.handleWebsocketError(err)
				return
			}

		case <-keepAliveTicker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				c.handleWebsocketError(err)
				return
			}
		}
	}
}

func (c *Client) handleWebsocketError(err error) {
	writeErr := c.conn.WriteJSON(sendMessage{Err: err.Error()})
	if writeErr != nil {
		c.logger.Sugar().Errorw("unable to send error message through websocket connection",
			"originalError", err,
			"websocketError", writeErr,
		)
	}
}

// ConnectChatClient upgrades an http connection to a websocket connection
// and then creates a new Client subscription.
//
// Websockets only support one concurrent reader and one concurrent writer
// so all reads are executed from 1 goroutine and all writes are executed
// from 1 goroutine.
func ConnectChatClient(logger *zap.Logger, chatServer *Server, roomName string, w http.ResponseWriter, r *http.Request) error {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return err
	}

	client := &Client{
		logger:      logger,
		id:          conn.RemoteAddr().String(),
		conn:        conn,
		server:      chatServer,
		currentRoom: roomName,
		send:        make(chan sendMessage, 256),
	}

	sub := subscription{
		roomName: roomName,
		client:   client,
	}

	client.server.joinChan <- sub

	go client.readFromSocket()
	go client.writeToSocket()

	return nil
}
