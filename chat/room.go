package chat

import (
	"sync"

	"bitbucket.org/nick_hultz/edge-swe-challenge/db"
	"bitbucket.org/nick_hultz/edge-swe-challenge/event"
	pb "bitbucket.org/nick_hultz/edge-swe-challenge/gen"
	"bitbucket.org/nick_hultz/edge-swe-challenge/model"
	"go.uber.org/zap"
)

// Room is a container that manages a single room and all its currently
// connected clients. Room controls broadcasting messages to
// all clients and manages the current room filters that apply
// to the incoming event feeds.
type Room struct {
	logger           *zap.Logger
	id               int
	name             string
	clients          map[*Client]bool
	clientsLock      sync.RWMutex
	chatStore        *db.ChatStore
	executionFilters []executionFilter
	sportFilters     []sportEventFilter
}

func (r *Room) addClient(client *Client) {
	r.clientsLock.Lock()
	defer r.clientsLock.Unlock()

	r.clients[client] = true

	r.logger.Sugar().Infow("client joined chat room",
		"client", client.id,
		"room", r.name,
	)
}

func (r *Room) removeClient(client *Client) {
	r.clientsLock.Lock()
	defer r.clientsLock.Unlock()

	if _, ok := r.clients[client]; ok {
		delete(r.clients, client)
		close(client.send)

		r.logger.Sugar().Infow("client left chat room",
			"client", client.id,
			"room", r.name,
		)
	}
}

func (r *Room) updateFilters(message changeFilter) {
	filters := message.ExecutionConfig.buildFilters()
	filters = append(filters, message.SportEventConfig.buildFilters()...)

	err := r.chatStore.SaveFilters(r.id, filters)
	if err != nil {
		r.logger.Sugar().Errorw("unable to persist room filter update",
			"client", message.ClientID,
			"room", r.name,
			"err", err,
		)
		return
	}

	r.logger.Sugar().Infow("chat room filters updated",
		"client", message.ClientID,
		"room", r.name,
	)

	r.refreshFilters()
}

func (r *Room) refreshFilters() {
	filters, err := r.chatStore.GetActiveFilters(r.id)
	if err != nil {
		r.logger.Sugar().Errorw("unable to retrieve active chat filters",
			"room", r.name,
			"err", err,
		)
		return
	}

	filterMap := make(map[string][]model.Filter)
	for _, f := range filters {
		filterMap[f.FeedType] = append(filterMap[f.FeedType], f)
	}

	executionFilters := []executionFilter{}
	if filters, exists := filterMap[event.ExecutionFeed]; exists {
		for _, f := range filters {
			filter, err := makeExecutionFilter(f)
			if err != nil {
				r.logger.Sugar().Errorw("error creating a chat filter",
					"feed", event.ExecutionFeed,
					"room", r.name,
					"err", err,
				)
				continue
			}
			executionFilters = append(executionFilters, filter)
		}
	}
	r.executionFilters = executionFilters
	r.logger.Sugar().Infow("chat filters refreshed",
		"feed", event.ExecutionFeed,
		"room", r.name,
	)

	sportEventFilters := []sportEventFilter{}
	if filters, exists := filterMap[event.SportEventFeed]; exists {
		for _, f := range filters {
			filter, err := makeSportEventFilter(f)
			if err != nil {
				r.logger.Sugar().Errorw("error creating a chat filter",
					"feed", event.SportEventFeed,
					"room", r.name,
					"err", err,
				)
				continue
			}
			sportEventFilters = append(sportEventFilters, filter)
		}
	}
	r.sportFilters = sportEventFilters
	r.logger.Sugar().Infow("chat filters refreshed",
		"feed", event.SportEventFeed,
		"room", r.name,
	)
}

func (r *Room) sendUserMessage(message sendMessage) {
	err := r.chatStore.SaveMessage(message.ClientID, message.Content, r.id)
	if err != nil {
		r.logger.Sugar().Errorw("unable to persist chat message",
			"client", message.ClientID,
			"message", message.Content,
			"room", r.name,
			"err", err,
		)
		return
	}

	// Iterating over all the connected clients needs to be synchronized
	// because messages can come from either the event feed goroutines or
	// the websocket connection.
	r.clientsLock.RLock()
	defer r.clientsLock.RUnlock()

	for client := range r.clients {
		select {
		case client.send <- message:
		default:
			close(client.send)
			delete(r.clients, client)
		}
	}
}

func (r *Room) broadcastExecution(execution *pb.Execution) {
	for _, filterFunc := range r.executionFilters {
		if !filterFunc(execution) {
			return
		}
	}

	msg := sendMessage{
		RoomName: r.name,
		Content:  execution.String(),
		ClientID: event.ExecutionFeed,
	}

	r.sendUserMessage(msg)
}

func (r *Room) broadcastSportEvent(sportEvent *pb.Event) {
	for _, filterFunc := range r.sportFilters {
		if !filterFunc(sportEvent) {
			return
		}
	}

	msg := sendMessage{
		RoomName: r.name,
		Content:  sportEvent.String(),
		ClientID: event.SportEventFeed,
	}

	r.sendUserMessage(msg)
}
