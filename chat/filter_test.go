package chat

import (
	"fmt"
	"testing"

	pb "bitbucket.org/nick_hultz/edge-swe-challenge/gen"
)

func TestEnabledExecutionFilter(t *testing.T) {
	sampleEvent := &pb.Execution{}

	var tests = []struct {
		given, expected bool
	}{
		{true, true},
		{false, false},
	}

	for _, test := range tests {
		testName := fmt.Sprintf("test enabled flag: %v", test.given)
		t.Run(testName, func(t *testing.T) {
			filterFunc := makeEnabledExecutionFilter(test.given)
			ans := filterFunc(sampleEvent)
			if ans != test.expected {
				t.Errorf("got %v, wanted %v", ans, test.expected)
			}
		})
	}
}

func TestExistsExecutionFilter(t *testing.T) {
	validMarkets := map[string]bool{
		"football": true,
		"golf":     true,
	}

	var tests = []struct {
		givenMarket string
		expected    bool
	}{
		{"football", true},
		{"tennis", false},
	}

	getFunc := func(e *pb.Execution) string { return e.GetMarket() }

	for _, test := range tests {
		testName := fmt.Sprintf("test exists: %v", test.givenMarket)
		t.Run(testName, func(t *testing.T) {
			filterFunc := makeExistsExecutionFilter(validMarkets, getFunc)

			sampleEvent := &pb.Execution{
				Market: &test.givenMarket,
			}
			ans := filterFunc(sampleEvent)
			if ans != test.expected {
				t.Errorf("got %v, wanted %v", ans, test.expected)
			}
		})
	}
}

func TestMaxValExecutionFilter(t *testing.T) {
	threshold := float32(25.8)

	var tests = []struct {
		givenPrice float32
		expected   bool
	}{
		{82.56, false},
		{12.5, true},
	}

	getFunc := func(e *pb.Execution) float32 { return e.GetPrice() }

	for _, test := range tests {
		testName := fmt.Sprintf("test exists: %v", test.givenPrice)
		t.Run(testName, func(t *testing.T) {
			filterFunc := makeMaxValExecutionFilter(threshold, getFunc)

			sampleEvent := &pb.Execution{
				Price: &test.givenPrice,
			}
			ans := filterFunc(sampleEvent)
			if ans != test.expected {
				t.Errorf("got %v, wanted %v", ans, test.expected)
			}
		})
	}
}

func TestEnabledSportEventFilter(t *testing.T) {
	sampleEvent := &pb.Event{}

	var tests = []struct {
		given, expected bool
	}{
		{true, true},
		{false, false},
	}

	for _, test := range tests {
		testName := fmt.Sprintf("test enabled flag: %v", test.given)
		t.Run(testName, func(t *testing.T) {
			filterFunc := makeEnabledSportEventFilter(test.given)
			ans := filterFunc(sampleEvent)
			if ans != test.expected {
				t.Errorf("got %v, wanted %v", ans, test.expected)
			}
		})
	}
}

func TestExistsSportEventFilter(t *testing.T) {
	validMarkets := map[string]bool{
		"football": true,
		"golf":     true,
	}

	var tests = []struct {
		givenSport pb.Sport
		expected   bool
	}{
		{pb.Sport_FOOTBALL, true},
		{pb.Sport_BOXING, false},
	}

	getFunc := func(e *pb.Event) string { return e.GetSport().String() }

	for _, test := range tests {
		testName := fmt.Sprintf("test exists: %v", test.givenSport)
		t.Run(testName, func(t *testing.T) {
			filterFunc := makeExistsSportEventFilter(validMarkets, getFunc)

			sampleEvent := &pb.Event{
				Sport: &test.givenSport,
			}
			ans := filterFunc(sampleEvent)
			if ans != test.expected {
				t.Errorf("got %v, wanted %v", ans, test.expected)
			}
		})
	}
}
