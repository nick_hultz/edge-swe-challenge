package chat

import (
	"fmt"

	"bitbucket.org/nick_hultz/edge-swe-challenge/db"
	"bitbucket.org/nick_hultz/edge-swe-challenge/event"
	"go.uber.org/zap"
)

type subscription struct {
	roomName string
	client   *Client
}

// Server is the middle man that coordinates chat messages
// between all the rooms.  Server is also responsible for
// subscribing to the event feeds and broadcasting the
// events to all the chat rooms.
type Server struct {
	logger        *zap.Logger
	rooms         map[string]*Room
	joinChan      chan subscription
	leaveChan     chan subscription
	userBroadcast chan sendMessage
	filterChan    chan changeFilter
	chatStore     *db.ChatStore
	eventListener *event.Listener
}

// NewServer creates a configured Server ready to run.
func NewServer(cs *db.ChatStore, l *event.Listener, logger *zap.Logger) *Server {
	return &Server{
		logger:        logger,
		rooms:         make(map[string]*Room),
		joinChan:      make(chan subscription),
		leaveChan:     make(chan subscription),
		userBroadcast: make(chan sendMessage),
		filterChan:    make(chan changeFilter),
		chatStore:     cs,
		eventListener: l,
	}
}

// CheckRoomExists validates the roomName against the list of valid rooms.
func (s *Server) CheckRoomExists(roomName string) error {
	if _, exists := s.rooms[roomName]; !exists {
		return fmt.Errorf("chat room named %q does not exist", roomName)
	}
	return nil
}

func (s *Server) loadRooms() {
	rooms, err := s.chatStore.GetRooms()
	if err != nil {
		s.logger.Sugar().Errorw("unable to load chat rooms from database",
			"err", err,
		)
		return
	}

	for _, room := range rooms {
		r := &Room{
			logger:    s.logger,
			id:        room.ID,
			name:      room.Name,
			clients:   make(map[*Client]bool),
			chatStore: s.chatStore,
		}
		r.refreshFilters()

		s.rooms[room.Name] = r
	}

	s.logger.Sugar().Info("chat rooms loaded")
}

// Run starts up the server and creates all the channel subscriptions
// that powers the chat server functionality.
func (s *Server) Run() {
	s.loadRooms()

	go func() {
		for msg := range s.eventListener.ExecutionChan {
			for _, room := range s.rooms {
				room.broadcastExecution(msg)
			}
		}
	}()

	go func() {
		for msg := range s.eventListener.SportEventChan {
			for _, room := range s.rooms {
				room.broadcastSportEvent(msg)
			}
		}
	}()

	for {
		select {
		case sub := <-s.joinChan:
			s.rooms[sub.roomName].addClient(sub.client)

		case sub := <-s.leaveChan:
			s.rooms[sub.roomName].removeClient(sub.client)

		case msg := <-s.userBroadcast:
			s.rooms[msg.RoomName].sendUserMessage(msg)

		case msg := <-s.filterChan:
			s.rooms[msg.RoomName].updateFilters(msg)
		}
	}
}
