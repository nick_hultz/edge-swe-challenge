#
# 1. Build Container
#
FROM golang:1.14 as builder

RUN mkdir /src

# First add modules list to better utilize caching
COPY go.sum go.mod /src/

WORKDIR /src

# Download dependencies
RUN go mod download

COPY . /src/

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o edge-swe-challenge .


#
# 2. Runtime Container
#
FROM alpine:latest

RUN apk --no-cache add ca-certificates

WORKDIR /app

COPY --from=builder /src/edge-swe-challenge .
COPY --from=builder /src/migrations ./migrations

CMD ["./edge-swe-challenge"]

EXPOSE 8080
