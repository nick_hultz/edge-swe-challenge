create table if not exists rooms (
   id serial primary key,
   name varchar (100) unique not null
);

create table if not exists messages (
   id serial primary key,
   user_name varchar (100) not null,
   room_id int not null,
   content varchar (2048) not null
);

create table if not exists filters (
   id serial primary key,
   room_id int not null,
   feed_type varchar(100) not null,
   filter_type varchar(100) not null,
   field varchar(100) not null,
   filter_value varchar(100) not null,
   active boolean not null
);
