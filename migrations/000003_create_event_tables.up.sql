create table if not exists feed_executions (
   id serial primary key,
   symbol varchar(50) not null,
   market varchar(100) not null,
   price numeric(10, 4) not null,
   quantity numeric(10, 4) not null,
   state_symbol varchar(5) not null,
   time_stamp bigint not null
);

create table if not exists feed_sport_events (
   id serial primary key,
   sport varchar(100) not null,
   match_title varchar(256) not null,
   data_event varchar(1024) not null,
   time_stamp bigint not null
);

