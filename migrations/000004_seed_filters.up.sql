insert into filters (room_id, feed_type, filter_type, field, filter_value, active)
values (
    (select id from rooms where name = 'room1'),
    'execution',
    'enabled',
    'enabled',
    'true',
    'true'
);

insert into filters (room_id, feed_type, filter_type, field, filter_value, active)
values (
    (select id from rooms where name = 'room2'),
    'execution',
    'enabled',
    'enabled',
    'true',
    'true'
);

insert into filters (room_id, feed_type, filter_type, field, filter_value, active)
values (
    (select id from rooms where name = 'room1'),
    'sport_event',
    'enabled',
    'enabled',
    'true',
    'true'
);

insert into filters (room_id, feed_type, filter_type, field, filter_value, active)
values (
    (select id from rooms where name = 'room2'),
    'sport_event',
    'enabled',
    'enabled',
    'true',
    'true'
);

