# Edge SWE Challenge

This application implements the following functionality:

-   Run a chat server where users can connect over a websocket connection and send messages to each other.
-   Subscribe to two feeds coming from a NATS broker and broadcast events to each chat room.
-   Allow users connected to a chat room to control which feed events they want the room to see.
-   Persist all NATS messages, chat messages, and chat room filters into a Postgres database.

## Running the application

The only prerequisite to running the application is to have docker / docker-compose installed.

```sh
docker-compose up -d --build
```

To stream the application logs to the terminal:

```sh
docker-compose logs -f chat
```

To use the chat service, connect the server (default port: `8080`) with a websocket connection at one of the two rooms:

-   `ws://localhost:8080/chat/room1`
-   `ws://localhost:8080/chat/room2`

_There are only two rooms seeded into the database at this point (`room1`, `room2`).  Multiple rooms are available to show that events in one room have no effect on other rooms._

## Chat Message Spec

The chat server supports two different types of messages over the websocket connection.

#### Send Chat Message

```json
{
    "type": "sendMessage",
    "msg": {
        "content": "Hello everyone!"
    }
}
```

#### Update Chat Filters

```json
{
    "type": "changeFilter",
    "msg": {
        "execution": {
            "enabled": true,
            "markets": [
                "football,golf,tennis,nascar,track&field,politics"
            ],
            "maxPrice": 30.00
        },
        "sportEvent": {
            "enabled": true,
            "sports": [
                "baseball,basketball,football,boxing,gold,nascar,tennis"
            ]
        }
    }
}
```

-   Filter behavior is as follows:
    -   The top level `enabled` field can be used to turn off an entire category of messages.
    -   List values represent the list of event types you want to see.
        -   For example: `"markets": ["football,golf"]` will only broadcast messages in the `football` or `golf` market.
    -   `maxPrice` filters all messages that go above that price threshold
    -   "Zero" values will turn the filter off completely:
        -   An empty list (`[]`)
        -   `0.00`
        -   `false`
    -   Any missing fields will use the default "zero" value.

## Architecture

The overall architecture of this application is a chat service that has multiple chat rooms that are independent of each other.  Each room can have any number of connected clients that can send messages to each other.

There are also two event feeds that are published into every chat room.  Any of the connected clients have the ability to update filters on their current room to control which event feed messages get published to that room.

All feed messages, chat messages, and filter changes are persisted into a Postgres database and will stay around through server restarts / client connections.

### Server

The `Server` is the middle-man between all the chat components.  It keeps a record of all the `Room`'s and coordinates messages between each of the `Client`'s.  The `Server` also listens for event messages coming from the `Listener` and publishes them into each `Room`.

### Client

The `Client` is the struct responsible for managing the connected websocket connection.  It handles reading messages from the websocket and publishing new messages to the websocket.

### Room

The `Room` is the struct responsible for managing a single chat room.  It keeps a record of all connected clients and is used to broadcast messages to all connected clients.  The other main responsibilty of the `Room` is to handle incoming `changeFilter` messages by persisting the new configuration into the database and then updating the active filters on the room that control which event feed messages get broadcast.

### Event Listener

The `Listener` in the `event` package is responsible for handling the connection to the NATS server and subscribing to the event feeds.  For every message that comes in, the `Listener` will save a record of it to the database, and then send it along to the `Server` to broadcast to all the rooms.

## Development

### Running Server Outside Of Docker

For faster local development, it is useful to only run the supporting services in Docker and run the application locally.

#### Bring up all supporting services

```sh
docker-compose up -d executions sports nats db
```

#### Run the application

```sh
go run main.go
```

### Running Tests

To run tests for all packages in the project

```sh
go test -v ./...
```

### Database Migrations

This project uses the [`golang-migrate`](https://github.com/golang-migrate/migrate) project to manage database migrations.  The easiest way to run migrations manually is through the Docker container.

The application will read the migrations directory on startup and attempt to run any outstanding migrations automatically.

#### Create a new migration

```sh
docker run -v $(pwd)/migrations:/migrations --network host migrate/migrate create -ext sql -dir /migrations -seq create_users_table
```

#### Run all outstanding migrations

```sh
docker run -v $(pwd)/migrations:/migrations --network host migrate/migrate -database "postgres://chat_svc:password@localhost:5432/chat?sslmode=disable" -path=/migrations up
```

#### Rollback a migration

```sh
docker run -v $(pwd)/migrations:/migrations --network host migrate/migrate -database "postgres://chat_svc:password@localhost:5432/chat?sslmode=disable" -path=/migrations down 1
```
